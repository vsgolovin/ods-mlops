# ods-mlops

Проект по курсу **MLOps и production в DS исследованиях 3.0**.
Я взял *getting-started* соревнование на Kaggle по предсказыванию цен домов -- [House Prices - Advanced Regression Techniques](https://www.kaggle.com/competitions/house-prices-advanced-regression-techniques/).
Пока здесь есть только не очень *advanced* линейная регрессия. :)

**Коммиты с домашними заданиями помечаются тэгами.**
Таким образом, чтобы просмотреть репозиторий на момент сдачи ДЗ по блоку 2, нужно найти тэг `block-2` на странице в GitLab или воспользоваться коммандой
```bash
git checkout block-2
```

## Установка

### Conda / mamba

```bash
conda env create -f environment.yml
conda activate ods
poetry install
```

### Docker (без jupyter-ноутбуков)

```bash
docker build . -t ods
docker run -v $(pwd):/app -u 0 -it ods
```
