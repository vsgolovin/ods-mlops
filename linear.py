import json
import pickle
from pathlib import Path

import click
import numpy as np
import pandas as pd
from sklearn.compose import TransformedTargetRegressor
from sklearn.linear_model import Ridge
from sklearn.metrics import make_scorer, r2_score
from sklearn.model_selection import KFold, cross_validate
from sklearn.preprocessing import QuantileTransformer

from src.utils import log_rmse


@click.group(context_settings={"show_default": True})
def cli():
    pass


@cli.command()
@click.option("--processed", type=click.Path(), default="data/processed")
@click.option("-o", "--output", type=click.Path(), default="models/linear.pkl")
@click.option("--json-output", type=click.Path(), default="")
def train(processed: str, output: str, json_output: str):
    processed = Path(processed)
    X = np.load(processed / "train.npy", allow_pickle=True)
    y = np.load(processed / "train_target.npy", allow_pickle=True)
    transformer = QuantileTransformer(output_distribution="normal")
    regr = TransformedTargetRegressor(Ridge(alpha=1.0), transformer=transformer)
    scores = cross_validate(
        regr,
        X,
        y,
        cv=KFold(10, shuffle=True, random_state=42),
        scoring={
            "r2": make_scorer(r2_score, greater_is_better=True),
            "rmse": make_scorer(log_rmse, greater_is_better=False),
        },
    )
    if json_output:
        with open("scores.json", "w") as fout:
            json.dump(scores, fout, indent=2)
    else:
        print(scores)

    r2 = scores["test_r2"]
    print(f"r2 = {r2.mean():.3f} +- {r2.std():.3f}")
    rmse = -scores["test_rmse"]
    print(f"rmse = {rmse.mean():.3f} +- {r2.std():.3f}")

    regr.fit(X, y)
    with open(output, "wb") as fout:
        pickle.dump(regr, fout)


@cli.command()
@click.option("-o", "--output", type=click.Path(), default="submissions/linear.csv")
@click.option("--test-data", type=click.Path(), default="data/processed/test.npz")
@click.option("--model", type=click.Path(), default="models/linear.pkl")
def predict(output: str, test_data: str, model: str):
    # load the model
    with open(model, "rb") as fin:
        regr = pickle.load(fin)

    # predict prices
    test = np.load(test_data)
    preds = regr.predict(test["features"])
    preds_df = pd.DataFrame(index=test["index"], data={"SalePrice": preds})
    preds_df.to_csv(output, index_label="Id")


if __name__ == "__main__":
    cli()
