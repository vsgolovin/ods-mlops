"""
Simple model that always predicts average price.
"""

import click
import pandas as pd


@click.group(context_settings={"show_default": True})
def cli():
    pass


@cli.command()
@click.option("-o", "--output", type=click.Path(), default="models/weak_baseline")
@click.option("--train-csv", default="data/raw/train.csv")
def train(output: str, train_csv: str):
    train = pd.read_csv(train_csv)
    mean = train["SalePrice"].mean()
    print(f"Average house price is {mean}")
    with open(output, "w") as fout:
        fout.write(str(mean))
        fout.write("\n")


@cli.command()
@click.option(
    "-o", "--output", type=click.Path(), default="submissions/weak_baseline.csv"
)
@click.option("--test-csv", type=click.Path(), default="data/raw/test.csv")
@click.option("--model", type=click.Path(), default="models/weak_baseline")
def predict(output: str, test_csv: str, model: str):
    # load the "model"
    with open(model, "r") as fin:
        price = float(fin.read())

    # predict average price
    test = pd.read_csv(test_csv)
    prediction = test.loc[:, ["Id"]]
    prediction["SalePrice"] = price

    # export predictions
    prediction.to_csv(output, index=False)


if __name__ == "__main__":
    cli()
