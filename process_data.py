from pathlib import Path

import click
import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.pipeline import FunctionTransformer, make_pipeline
from sklearn.preprocessing import KBinsDiscretizer, OneHotEncoder, RobustScaler


@click.command(context_settings={"show_default": True})
@click.option("--raw", type=click.Path(), default="data/raw")
@click.option("--interim", type=click.Path(), default="data/interim")
@click.option("--processed", type=click.Path(), default="data/processed")
def main(raw: str, interim: str, processed: str):
    raw, interim, processed = map(Path, [raw, interim, processed])

    # preprocessing with pandas
    train = pd.read_csv(raw / "train.csv")
    test = pd.read_csv(raw / "test.csv")
    new_feats_train, _ = preprocess_dataframe(train)
    new_feats_test, _ = preprocess_dataframe(test)
    assert new_feats_train == new_feats_test

    # export interim data
    train.to_csv(interim / "train.csv", index=False)
    test.to_csv(interim / "test.csv", index=False)

    # preprocessing with sklearn
    ct = get_column_transformer()
    train_arr = ct.fit_transform(train)
    test_arr = ct.transform(test)
    feats_after = ct.get_feature_names_out()
    assert all([feat in feats_after for feat in new_feats_train])

    # export final data
    np.save(processed / "train.npy", train_arr)
    np.save(processed / "train_target.npy", train["SalePrice"].to_numpy())
    np.savez(processed / "test.npz", features=test_arr, index=test["Id"].to_numpy())
    with open(processed / "features.txt", "w") as fout:
        for feat in feats_after:
            fout.write(feat + "\n")


def preprocess_dataframe(
    df: pd.DataFrame, fit: bool = True
) -> tuple[list[str], list[str]]:
    """Add new features to a dataframe before doing any other transformations.
    Should be data-leakage-safe: no filling NaNs with median, no scaling with std, etc.

    Args:
        df (pd.DataFrame): Input data, will be modified in-place.

    Returns:
        list[str]: Added features, needed for further processing.
        list[str]: Modified features / dataframe columns with modified values.
    """
    new_features = []
    modified_features = []

    def add_feature(name, values):
        if fit:
            assert values.any()
        df[name] = values
        new_features.append(name)

    add_feature("Alley_Grvl", df["Alley"] == "Grvl")
    add_feature("LotShape_IR", df["LotShape"].str.startswith("IR"))
    add_feature("BldgType_Cheap", df["BldgType"].isin(["2fmCon", "Duplex", "TwnhsE"]))
    add_feature("Age", df["YrSold"] - df["YearBuilt"])
    remodelled = df["YearRemodAdd"] - df["YearBuilt"]
    add_feature("Remodelled_1", remodelled == 1)  # more expensive for some reason
    add_feature("Remodelled_2+", (df["YearRemodAdd"] - df["YearBuilt"]) > 1)
    add_feature("MasVnrType_BrkFace", df["MasVnrType"] == "BrkFace")
    add_feature("MasVnrType_Stone", df["MasVnrType"] == "Stone")
    add_feature("MasVnrArea_nonzero", df["MasVnrArea"] > 0)
    add_feature("CentralAir_N", df["CentralAir"] == "N")
    add_feature("Electrical_Fuse", df["Electrical"].fillna("na").str.startswith("Fuse"))
    add_feature("LowQualFinSF_nonzero", df["LowQualFinSF"] > 0)
    add_feature("BedroomAbvGr_3+", df["BedroomAbvGr"] >= 3)
    add_feature("KitchenAbvGr_2+", df["KitchenAbvGr"] >= 2)
    add_feature("Functional_Deductions", ~(df["Functional"] == "Typ"))  # nan == Typ
    add_feature("GarageType_Detchd", df["GarageType"] == "Detchd")
    add_feature("GarageType_BuiltIn", df["GarageType"] == "BuiltIn")
    df.loc[df["GarageCars"] > 3, "GarageCars"] = 3  # 3 is actually 3+ now
    modified_features.append("GarageCars")
    add_feature("SaleType_New", df["SaleType"] == "New")

    return (new_features, modified_features)


def get_column_transformer():
    return ColumnTransformer(
        [
            (
                "robust_scaler",
                make_pipeline(SimpleImputer(strategy="median"), RobustScaler()),
                ["LotFrontage", "LotArea", "Age"],
            ),
            (
                "scale_1_to_10",
                make_pipeline(
                    FunctionTransformer(
                        lambda x: (x - 5) / 5.0,
                        feature_names_out="one-to-one",
                        validate=True,
                    ),
                    SimpleImputer(fill_value=0),
                ),
                ["OverallQual", "OverallCond"],
            ),
            (
                "log_features",
                make_pipeline(
                    SimpleImputer(strategy="median"),
                    FunctionTransformer(
                        lambda x: np.log(x + 1),
                        feature_names_out="one-to-one",
                        validate=True,
                    ),
                ),
                ["TotalBsmtSF", "GrLivArea"],
            ),
            (
                "ohe",
                OneHotEncoder(min_frequency=50, handle_unknown="ignore", drop="first"),
                [
                    "MSSubClass",
                    "MSZoning",
                    "HouseStyle",
                    "Exterior1st",
                    "Foundation",
                    "CentralAir",
                    "FullBath",
                    "HalfBath",
                    "GarageCars",
                ],
            ),
            (
                "kbins",
                make_pipeline(
                    SimpleImputer(strategy="median"),
                    KBinsDiscretizer(strategy="quantile"),
                ),
                ["TotRmsAbvGrd"],
            ),
            (
                "neighborhood",
                OneHotEncoder(
                    categories=[["NAmes", "OldTown", "Edwards", "NridgHt", "Sawyer"]],
                    handle_unknown="ignore",
                ),
                ["Neighborhood"],
            ),
            (
                "passthrough",
                "passthrough",
                [
                    "Alley_Grvl",
                    "LotShape_IR",
                    "BldgType_Cheap",
                    "Remodelled_1",
                    "Remodelled_2+",
                    "MasVnrType_BrkFace",
                    "MasVnrType_Stone",
                    "MasVnrArea_nonzero",
                    "CentralAir_N",
                    "Electrical_Fuse",
                    "LowQualFinSF_nonzero",
                    "BedroomAbvGr_3+",
                    "KitchenAbvGr_2+",
                    "Functional_Deductions",
                    "GarageType_Detchd",
                    "GarageType_BuiltIn",
                    "SaleType_New",
                ],
            ),
        ],
        remainder="drop",
        verbose_feature_names_out=False,
    )


if __name__ == "__main__":
    main()
