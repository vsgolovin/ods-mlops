## Установка зависимостей

см. [README.md](README.md)

## Линтеры

Я использую стандартную связку `flake8`+`isort`+`black` c настройками по
умолчанию.  Дополнительно используется библиотека `Flake8-pyproject`,
позволяющая настраивать `flake8` в `pyproject.toml`.  Версии этих библиотек
указаны в `pyproject.toml`.  В настройках `flake8` изменена только
максимальная длина строки -- с 79 до 88 символов (значение по умолчанию в
`black`).

Для автоматической работы линтеров при сохранении файлов в `vscode` необходимо
установить соответствующие дополнения от Microsoft, а также добавить следующие
поля в `settings.json`:
```json
"[python]": {
    "editor.defaultFormatter": "ms-python.black-formatter",
    "editor.formatOnSave": true,
    "editor.codeActionsOnSave": {
        "source.organizeImports": "always"
    }
},
"isort.args": ["--profile", "black"],
```

Aвтоматический запуск проверок с помощью `pre-commit` при выполнении `git
commit` / `git push` включается коммандой
```bash
pre-commit install
```
Чтобы `pre-commit` тесты работали при использовании GUI `vscode`, нужно
установить одноименное расширение.
