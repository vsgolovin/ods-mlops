import numpy as np
from sklearn.metrics import root_mean_squared_error


def log_rmse(y_true, y_pred):
    return root_mean_squared_error(np.log(y_true), np.log(y_pred))
