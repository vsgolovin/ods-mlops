FROM mambaorg/micromamba

WORKDIR /app

COPY environment.yml ./
RUN micromamba config set extract_threads 1
RUN micromamba install -y -n base -f environment.yml && \
    micromamba clean --all --yes

COPY pyproject.toml ./
COPY poetry.lock ./
RUN micromamba run poetry config virtualenvs.create false
RUN micromamba run poetry install
